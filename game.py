from random import randint

name = input("Hi! What is your name?")

guess_month = randint(1,12)
guess_year = randint(1924,2004)

for attempt in range(0,5):

    print("Guess ", attempt + 1, " : ", name, " were you born in ", guess_month, " / ", guess_year, "?")
    answer = input("yes or no?")

    if attempt < 4 and answer == "no":
        print("Drat! Lemme try again!")
        guess_month = randint(1,12)
        guess_year = randint(1924,2004)

    if attempt == 4 and answer == "no":
        print("I have other things to do. Good bye.")
        exit()

    if answer == "yes":
        print("I knew it!")
        exit()
